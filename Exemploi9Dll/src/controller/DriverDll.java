package controller;

import java.util.Scanner;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.*;

public class DriverDll {
	
	int f;
	byte[] retorno;
	String texto;
	private FuncoesDll dll;
	private PointerByReference pr = new PointerByReference();
	private Pointer p;
	private IntByReference ip = new IntByReference();
	
//	Apontando DLL usando um TRY CATCH.
	public DriverDll() {
		try {
			dll = (FuncoesDll) Native.loadLibrary("HprtPrinter.dll", FuncoesDll.class);
			System.out.println("DLL Carregada");
		} catch (Exception e) {
			System.out.println("Erro.");
		}
	}
	public void criarImpressora() {
		f = dll.PrinterCreator(pr, "i9");
		p = pr.getValue();
	}
	public void abrirPorta() {
		f = dll.PortOpen(p, "USB");
		p = pr.getValue();
	}
	public int menuPrincipal() {
		System.out.println("**************************************************\n");
        System.out.println("*                                                *\n");
        System.out.println("*           EXEMPLO I9 - COMANDO DIRETO          *\n");
        System.out.println("*                                                *\n");
        System.out.println("**************************************************\n\n\n");
        System.out.println("�   [1]    IMPRESS�O CUPOM PADR�O - ELGIN.\n");
        System.out.println("�   [2]    ABRIR GAVETA.\n");
        System.out.println("�   [3]    STATUS DE IMPRESSORA, PAPEL E GAVETA.\n");
        System.out.println("�   [4]    IMPRESS�O CUPOM LAYOUT NOVO - ELGIN.\n");
        System.out.println("�   [5]    IMPRESS�O DE TODOS OS CODE128\n");
        System.out.println("�   [6]    IMPRESS�O DE TODAS AS FONTES E TAMANHOS.\n");
        System.out.println("�   [7]    FINALIZAR.\n");
        
        Scanner teclado = new Scanner(System.in);
        int tecla = teclado.nextInt();
        
        switch (tecla) {
        case 1: cupomPadrao();
        	break;
        	
        case 2: comandoGaveta();
        	break;
        	
        case 3: statusImpressora();
        	break;
        	
        case 4: cupomLayoutNovo();
        	break;
        	
        case 5: todosCodigos();
        	break;
        	
        case 6: testeFontes();
        	break;
        	
        case 7: fechandoComunicacao();
        	break;
        }
		return tecla;
	}
	public void cupomPadrao() {
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "Elgin Manaus";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "Elgin S/A";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "Rua Abiurana, 579 Distrito Industrial Manaus - AM";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 2);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "CNPJ 14.200.166/0001-66 IE 111.111.111.111 IM";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "----------------------------------------------------------------";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "EXTRATO No. 002046";
		f = dll.PrintText(p, texto, 1, 2, 0);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "CUPOM FISCAL ELETRONICO - SAT";
		f = dll.PrintText(p, texto, 1, 2, 0);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "----------------------------------------------------------------";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "CPF/CNPJ consumidor:";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "----------------------------------------------------------------";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "# | COD | DESC | QTD | UN | VL UN R$ | (VL TR r$)* | VL ITEM r$";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "----------------------------------------------------------------";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "| 1000016  VRAAAAAAAAAAAAAAAAAAAAAAU 3,449 L 2,9++ (3.84)  10.00";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 2);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "Subtotal                                                   10,00";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "TOTAL R$                                                   10,00";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 2);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "Dinheiro                                                   10,00";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "Troco R$                                                    0,00";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 2);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "----------------------------------------------------------------";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "Tributos Totais (Lei Fed 12.741/12) R$                      3,85";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "----------------------------------------------------------------";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 0);
		texto = "R$ 3,84 Trib aprox R$ 1,35 Fed R$ 2,50 Est";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 0);
		texto = "Fonte: IBPT/FECOMERCIO (RS) 9oi3aC";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 2);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 0);
		texto = "KM: 0";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "----------------------------------------------------------------";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 0);
		texto = "* Valor aproximado dos tributos do item";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "----------------------------------------------------------------";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "SAT No. 900001231";
		f = dll.PrintText(p, texto, 1, 2, 0);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "01/01/2018 - 00:00:00";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 2);
		
//		CODE128
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "{B35150661099008000141593515066109900800014159";
		f = dll.PrintBarCode(p, 73, texto, 1, 60, 1, 0);
		f = dll.FeedLine(p, 3);
		
//		QR CODE
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "http://homnfce.sefaz.am.gov.br/nfceweb/consultarNFCe.jsp?chNFe=13161104240370000742652000000445901000445906&nVersao=100&tpAmb=2&dhEmi=323031362D31312D31375430393A33373A31372D30323A3030&vNF=11.98&vICMS=0.00&digVal=497A762B634A4439374258533568546751753541375330554E436B3D&cIdToken=000001&cHashQRCode=2A1303EDAEFAB7345F841BDC7A6BD75B1DD0866E";
		f = dll.PrintSymbol(p, 103, texto, 48, 4, 0, 1);
		f = dll.FeedLine(p, 2);
		f = dll.CutPaper(p, 0, 0);
	}
	public void comandoGaveta() {
		f = dll.OpenCashDrawer(p, 0, 100, 100);
	}
	public void statusImpressora() {
		f = dll.GetPrinterState(p, ip);
		switch(ip.getValue()) {
		case 0:
			System.out.println("---------------");
			System.out.println("STATUS: NORMAL.");
			System.out.println("---------------");
			break;
		case 1:
			System.out.println("------------------");
			System.out.println("STATUS: SEM PAPEL.");
			System.out.println("------------------");
			break;
		case 2:
			System.out.println("---------------------");
			System.out.println("STATUS: TAMPA ABERTA.");
			System.out.println("---------------------");
			break;
		case 4:
			System.out.println("------------------------------");
			System.out.println("STATUS: PAPEL PERTO DE ACABAR.");
			System.out.println("------------------------------");
			break;
		case 32:
			System.out.println("-------------------------------");
			System.out.println("STATUS: ERRO AO OBTER O STATUS.");
			System.out.println("-------------------------------");
			break;
		case 64:
			System.out.println("----------------------");
			System.out.println("STATUS: PORTA FECHADA.");
			System.out.println("----------------------");
			break;
		case 128:
			System.out.println("-----------------------------");
			System.out.println("STATUS: IMPRESSORA DESLIGADA.");
			System.out.println("-----------------------------");
			break;
		}
	}
	public void cupomLayoutNovo() {
		
		
		
	}
	public void todosCodigos() {
		
		
//		CODE UPCA
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "01234567012";
		f = dll.PrintBarCode(p, 65, texto, 1, 60, 1, 0);
		f = dll.FeedLine(p, 2);
		
//		CODE UPCE
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "01234567890";
		f = dll.PrintBarCode(p, 66, texto, 1, 60, 1, 0);
		f = dll.FeedLine(p, 2);
		
//		CODE EAN8
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "01234567";
		f = dll.PrintBarCode(p, 68, texto, 1, 60, 1, 0);
		f = dll.FeedLine(p, 2);
		
//		CODE CODE39 altera��o de teste
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "*0123456789*";
		f = dll.PrintBarCode(p, 69, texto, 1, 60, 1, 0);
		f = dll.FeedLine(p, 2);
		
//		CODE ITF
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "0123456789";
		f = dll.PrintBarCode(p, 70, texto, 1, 60, 1, 0);
		f = dll.FeedLine(p, 2);
		
//		CODEBAR
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "A40156B";
		f = dll.PrintBarCode(p, 71, texto, 1, 60, 1, 0);
		f = dll.FeedLine(p, 2);
		
//		CODEB93
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "*TESTECODE93*";
		f = dll.PrintBarCode(p, 72, texto, 1, 60, 1, 0);
		f = dll.FeedLine(p, 2);
		
//		CODE128
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		f = dll.SetAlign(p, 1);
		texto = "{B35150661099008000141593515066109900800014159";
		f = dll.PrintBarCode(p, 73, texto, 1, 60, 1, 0);
		f = dll.FeedLine(p, 3);
		f = dll.CutPaper(p, 0, 0);
		
	}
	public void testeFontes() {
		
//		ALTURAS
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Altura 1";
		f = dll.PrintText(p, texto, 0, 0, 0);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Altura 2";
		f = dll.PrintText(p, texto, 0, 0, 1);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Altura 3";
		f = dll.PrintText(p, texto, 0, 0, 2);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Altura 4";
		f = dll.PrintText(p, texto, 0, 0, 3);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Altura 5";
		f = dll.PrintText(p, texto, 0, 0, 4);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Altura 6";
		f = dll.PrintText(p, texto, 0, 0, 5);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Altura 7";
		f = dll.PrintText(p, texto, 0, 0, 6);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Altura 8";
		f = dll.PrintText(p, texto, 0, 0, 7);
		f = dll.FeedLine(p, 1);
		
//		LARGURAS
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Largura 1";
		f = dll.PrintText(p, texto, 0, 0, 0);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Largura 2";
		f = dll.PrintText(p, texto, 0, 0, 16);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Largura 3";
		f = dll.PrintText(p, texto, 0, 0, 32);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Largura 4";
		f = dll.PrintText(p, texto, 0, 0, 48);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Largura 5";
		f = dll.PrintText(p, texto, 0, 0, 64);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Largura 6";
		f = dll.PrintText(p, texto, 0, 0, 80);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Largura 7";
		f = dll.PrintText(p, texto, 0, 0, 96);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Largura 8";
		f = dll.PrintText(p, texto, 0, 0, 112);
		f = dll.FeedLine(p, 2);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Com Negrito";
		f = dll.PrintText(p, texto, 0, 2, 0);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Sem Negrito";
		f = dll.PrintText(p, texto, 0, 0, 0);
		f = dll.FeedLine(p, 2);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "MiniFont";
		f = dll.PrintTextS(p, texto);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Fonte Normal";
		f = dll.PrintText(p, texto, 0, 0, 0);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 0);
		texto = "Fonte C";
		f = dll.PrintText(p, texto, 0, 0, 0);
		f = dll.FeedLine(p, 2);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Sublinhado";
		f = dll.PrintText(p, texto, 0, 4, 0);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Sem Sublinhado";
		f = dll.PrintText(p, texto, 0, 0, 0);
		f = dll.FeedLine(p, 2);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Alinhado a Esquerda";
		f = dll.PrintText(p, texto, 0, 0, 0);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Centralizado";
		f = dll.PrintText(p, texto, 1, 0, 0);
		f = dll.FeedLine(p, 1);
		
		f = dll.PrinterInitialize(p);
		f = dll.SetTextFont(p, 1);
		texto = "Alinhado a Direita";
		f = dll.PrintText(p, texto, 2, 0, 0);
		f = dll.FeedLine(p, 2);
		f = dll.CutPaper(p, 0, 0);
		
//		CARACTERES ESPECIAIS
//		
//		f = dll.PrinterInitialize(p);
//		f = dll.SetCodePage(p, 850, 0);
//		f = dll.SetTextFont(p, 1);
//		texto = "�������������������";
//		f = dll.PrintText(p, texto, 0, 0, 0);
//		f = dll.FeedLine(p, 1);
//		
//		f = dll.PrinterInitialize(p);
//		f = dll.SetCodePage(p, 850, 0);
//		f = dll.SetTextFont(p, 1);
//		texto = "�������������������";
//		f = dll.PrintText(p, texto, 0, 2, 0);
//		f = dll.FeedLine(p, 1);
//		
//		f = dll.PrinterInitialize(p);
//		f = dll.SetCodePage(p, 850, 0);
//		f = dll.SetTextFont(p, 1);
//		texto = "�������������������";
//		f = dll.PrintText(p, texto, 0, 4, 0);
//		f = dll.FeedLine(p, 1);
//		
//		f = dll.PrinterInitialize(p);
//		f = dll.SetCodePage(p, 850, 0);
//		f = dll.SetTextFont(p, 1);
//		texto = "�������������������";
//		f = dll.PrintText(p, texto, 0, 8, 0);
//		f = dll.FeedLine(p, 2);
		
		
	}
	public void fechandoComunicacao() {
		
		f = dll.PrinterDestroy(p);
		System.out.println("Sess�o Finalizada.");
	}
}
