package controller;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

public interface FuncoesDll extends Library{
	
	public int PrinterCreator(PointerByReference p, String model);
	public int PrinterDestroy(Pointer p);
	public int PortOpen(Pointer p, String ioSettings );
	public int PortClose(Pointer p);
	public int PrinterInitialize(Pointer p);
	public int SetTextLineSpace(Pointer p, int lineSpace);
	public int CancelPrintDataInPageMode(Pointer p);
	public int GetPrinterState(Pointer p, IntByReference printerStatus);
	public int SetCodePage(Pointer p, int characterSet, int type);
	public int SetInternationalCharacter(Pointer p, int characterSet);
	public int CutPaper(Pointer p, int cutMode, int distance);
	public int FeedLine(Pointer p, int lines);
	public int OpenCashDrawer(Pointer p, int pinMode, int onTime, int  offTime);
	public int PrintText(Pointer p, String data, int alignment, int attribute, int textSize);
	public int PrintTextS(Pointer p, String data);
	public int PrintBarCode(Pointer p, int bcType, String bcData, int width, int height, int aligment, int hriPosition);
	public int PrintSymbol(Pointer p, int type, String data, int errLevel, int width, int height, int aligment);
	public int PrintTwoQRCode(Pointer p, char[] data1, int data1Len, int width1, int hAlign1, int vAlign1, char[] data2, int data2Len, int width2, int hAlign2, int vAlign2);
	public int PrintImage(Pointer p, char[] filePath, int scaleMode);
	public int PrintBitMapData(Pointer p, int scaleMode, int width, int height, byte data);
	public int DefineNVImageCompatible(Pointer p, char[] filePathList, int imageQty);
	public int PrintNVImageCompatible(Pointer p, int imgNo, int scaleMode);
	public int DefineDownloadedImageCompatible(Pointer p, char[] filePath);
	public int PrintDownloadedImageCompatible(Pointer p, int scaleMode);
	public int GetFirmwareVersion(Pointer p, int version, int versionLen);
	public int SelectPageMode(Pointer p);
	public int SelectStandardMode(Pointer p);
	public int SelectPrintDirectionInPageMode(Pointer p, int direction);
	public int SetAbsoluteVerticalPrintPositionInPageMode(Pointer p, int position);
	public int SetPrintAndReturnStandardMode(Pointer p);
	public int SetPrintAreaInPageMode(Pointer p, int horizontal, int vertical, int width, int height);
	public int PrintDataInPageMode(Pointer p);
	public int DirectIO(Pointer p, byte[] writeData, int writeNum, byte[] readData, int readNum, int preadedNum);
	public int SetAbsolutePrintPosition(Pointer p, int position);
	public int PositionNextLabel(Pointer p);
	public int DefineNVImage(Pointer p, char[] imagePath, byte kc1, byte kc2);
	public int PrintNVImage(Pointer p, byte kc1, byte kc2);
	public int DefineDownloadedImage(Pointer p, char[] imagePath, byte kc1, byte kc2);
	public int DefineBufferedImage(Pointer p, char[] imagePath);
	public int PrintBufferedImage(Pointer p);
	public int DeleteAllNVImages(Pointer p);
	public int GetCashDrawerState(Pointer p, int drawerState);
	public int ClearBuffer(Pointer p);
	public int FormatError(int errorNo, int langid, byte buf, int pos, int bufSize);
	public int SetTextBold(Pointer p, int bold);
	public int SetTextFont(Pointer p, int font);
	public int SetBuzzer(Pointer p, int enable);
	public int SetLog(int enable, char[] path);
	public int SetAlign(Pointer p, int align);
	

}
